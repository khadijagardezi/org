---
title: Utilities
description: An overview of the Software Tools we use at Grey Software
category: Tools and Tech
position: 20
---

## Brave Browser and Firefox

![Browsers Preview](/tech-stack/browsers-preview.png)

Most of our projects management and development happens on web browsers, and there are two major open source engines in the market right now: Chromium and Firefox Quantum.

Add Grey software, we regularly use both browser engines for diverse compatability, and our favorite browsers to work with are Mozilla Firefox and Brave.

<cta-button  link="https://brave.com/download/" text="Brave Browser" > </cta-button> <cta-button  link="https://www.mozilla.org/en-US/firefox/new/" text="Download Firefox" > </cta-button>
